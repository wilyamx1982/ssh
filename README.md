# SSH CLI

[ssh-keygen](https://linux.die.net/man/1/ssh-keygen)

## ssh server connect

	$ ssh -i ~/.pems/albert-stg.pem ubuntu@worker-stg.albert.today
	
	$ ssh -i ~/.pems/albert-prod.pem ubuntu@worker.albert.today
	$ ssh -i ~/.pems/dtp-prod.pem ubuntu@worker.dailytrainingpartner.com
	
## ssh-keygen

	$ ssh-keygen -f ~/.ssh/known_hosts -R worker-stg.albert.today
	
	$ ssh-keygen -f ~/.ssh/known_hosts -R worker.albert.today
	$ ssh-keygen -f ~/.ssh/known_hosts -R worker.dailytrainingpartner.com
	
## set-up ssh key

	$ ssh-keygen 
	Generating public/private rsa key pair.
	Enter file in which to save the key (/Users/emmap1/.ssh/id_rsa):
	Created directory '/Users/emmap1/.ssh'.
	Enter passphrase (empty for no passphrase):
	Enter same passphrase again:
	Your identification has been saved in /Users/emmap1/.ssh/id_rsa.
	Your public key has been saved in /Users/emmap1/.ssh/id_rsa.pub.
	The key fingerprint is:
	4c:80:61:2c:00:3f:9d:dc:08:41:2e:c0:cf:b9:17:69 emmap1@myhost.local 
	The key's randomart image is:
	+--[ RSA 2048]----+
	|*o+ooo.          |
	|.+.=o+ .         |
	|. *.* o .        |
	| . = E o         |
	|    o . S        |
	|   . .           |
	|     .           |
	|                 |
	|                 |
	+-----------------+       
	
	$ ls ~/.ssh 
	id_rsa  id_rsa.pub  
	
	$ ssh -T git@bitbucket.org
	$ ssh -T git@github.com
	
	$ cat ~/.ssh/id_rsa.pub
	$ pbcopy < ~/.ssh/id_rsa.pub    